const fs = require('fs');
const path = './messages/';

let data = [];

module.exports = {
    init() {
        try {
            fs.readdir(path, (err, files) => {
                files.forEach(file => {
                    data.push(JSON.parse(fs.readFileSync(path + file)));
                    console.log(path + file);
                });
            })
        } catch (e) {
            data = [];
        }
    },
    getItems() {
        return data.slice(Math.max(data.length - 5, 1));
    },
    save(message) {
        fs.writeFileSync(path + new Date() + '.txt', JSON.stringify(message, null, 2));
    }
};
