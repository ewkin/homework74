const express = require('express');
const fileDb = require('./fileDb');
const app = express();

fileDb.init();

const port = 8000;
app.use(express.json());

app.get('/messages', (req, res) => {
    fileDb.init();
    res.send(fileDb.getItems());
});


app.post('/messages', (req, res) => {
    let message = req.body;
    message.datetime = new Date();
    fileDb.save(message);
    res.send(JSON.stringify(message, null, 2));
});

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});